var host = "http://46.101.182.219/api/v1";

var getSlug = function(callback) {
	var slug = '';
	$.ajax({
		url: 'video_key',
		type: 'GET',
		success: function(data) {
			callback(data);
		},
		error: function(data) {
			swal('Error !', 'Error in video name !', 'error');
		}
	});
	// return slug;
	// return 'v-1447820623499';
};

$(function() {
	$('#key_form').submit(function(event) {
		event.preventDefault();
		var key = $('#key').val();
		var slug = '';
		console.info('setting slug');
		getSlug(function(data) {
			slug = data;

			if (isValidKey(key)) {
				checkWithServer(key, slug, function() {
					window.location = 'video.html?key=' + key;
				});
			} else {
				swal('Error!', 'Key is not valid.', 'error');
			}
		});

	});

	var isValidKey = function(key) {
		return key.length === 7;
	};

	var checkWithServer = function(key, slug, callback) {
		console.info(host + '/key/check');
		$.ajax({
			url: host + '/key/check',
			type: 'POST',
			crossDomain: true,
			data: {
				key: key,
				slug: slug
			},
			success: function(data) {
				callback();
			},
			error: function(err) {
				console.warn(err);
				swal('Error!', 'Key is wrong or expired !', 'error');
			}
		});

	};
});

var getQueryParams = function() {
	qs = document.location.search.split('+').join(' ');

	var params = {},
		tokens,
		re = /[?&]?([^=]+)=([^&]*)/g;

	while ((tokens = re.exec(qs))) {
		params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
	}

	return params;
};