var host = "http://46.101.182.219/api/v1";

$(function() {
	var key = getQueryParams().key;
	var slug = '';
	console.info('setting slug');
	getSlug(function(data) {
		slug = data;

		checkWithServer(key, slug, function(remaining_views) {
			// If key is valid. start to decode
			decodeVideoStuff();

			$('#remaining_views').text(remaining_views);
		});

		setInterval(function() {
			console.info('testing');
			checkWithServer(key, slug, function(remaining_views) {
				// If key is valid. start to decode
				decodeVideoStuff();

				$('#remaining_views').text(remaining_views);
			});
		}, 10 * 1000); // every 3 minutes
	});




});

var decodeVideoStuff = function() {
	// Video JS stuff to init player w kda.
	videojs('videoPlayer')
		.ready(function() {
			this.hotkeys({
				volumeStep: 0.1,
				seekStep: 1,
				enableMute: true,
				enableFullscreen: true,
			});
		});
};

var getQueryParams = function() {
	var uri = document.location.search;

	qs = uri.split('+').join(' ');

	var params = {},
		tokens,
		re = /[?&]?([^=]+)=([^&]*)/g;

	while ((tokens = re.exec(qs))) {
		params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
	}

	return params;
};


var getSlug = function(callback) {
	var slug = '';
	$.ajax({
		url: 'video_key',
		type: 'GET',
		success: function(data) {
			callback(data);
		},
		error: function(data) {
			swal('Error !', 'Error in video name !', 'error');
		}
	});
	// return slug;
	// return 'v-1447820623499';
};

var checkWithServer = function(key, slug, callback) {
	$.ajax({
		url: host + '/key/check',
		type: 'POST',
		crossDomain: true,
		data: {
			key: key,
			slug: slug
		},
		success: function(data) {
			callback(data.remaining_views);
		},
		error: function(err) {
			console.warn(err);
			swal('Error!', err, 'error');
			window.location = "/";
		}
	});

};