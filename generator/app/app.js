var express = require('express');
var fs = require("fs");
var url = require("url");
var path = require("path");

var app = express();

app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/assets', express.static(__dirname + '/public/assets'));
app.use('/public', express.static(__dirname + '/public'));


var pages = {
	index: path.resolve(__dirname, "public", "index.html"),
	video: path.resolve(__dirname, "public", "video.html")
};

var INFO_START_BYTE = 16178;
var INFO_LENGTH = 15;
var VIDEO_START_BYTE = INFO_START_BYTE + INFO_LENGTH;

var FILE = path.resolve(__dirname, "data");

app.get('/', function(req, res) {
	res.sendFile(pages.index);
});

app.get('/video.html', function(req, res) {
	res.sendFile(pages.video);
});


app.get('/video_key', function(req, res) {
	fs.createReadStream(FILE, {
		"start": INFO_START_BYTE,
		"end": INFO_START_BYTE + INFO_LENGTH
	})
		.on('data', function(buffer) {
			res.send(new Buffer(buffer, 'hex').toString('utf-8'));
		});
});

app.get('/v.mp4', function(req, res) {
	console.log("asd");
	var range = req.headers.range;
	var positions = range.replace(/bytes=/, "").split("-");
	var start = parseInt(positions[0], 10);
	var offset = VIDEO_START_BYTE;

	fs.stat(FILE, function(err, stats) {
		var total = stats.size - offset;
		var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
		var chunkSize = (end - start) + 1;

		res.writeHead(206, {
			"Content-Range": "bytes " + start + "-" + end + "/" + total,
			"Accept-Ranges": "bytes",
			"Content-Length": chunkSize,
			"Content-Type": "video/mp4"
		});

		fs.createReadStream(FILE, {
			start: start + offset,
			end: end + offset
		})
			.on("open", function() {
				this.pipe(res); // "this" refers to current readStream.
			})
			.on("error", function(err) {
				res.end(err);
			});
	});
});

app.listen(7997);