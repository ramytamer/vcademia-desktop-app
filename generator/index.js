var host = "http://46.101.182.219/api/v1";

(function() {
	var gui = require('nw.gui');


	var slugify = function(text) {
		slug = text.toString().toLowerCase()
			.replace(/\s+/g, '-') // Replace spaces with -
		.replace(/[^\w\-]+/g, '') // Remove all non-word chars
		.replace(/\-\-+/g, '-') // Replace multiple - with single -
		.replace(/^-+/, '') // Trim - from start of text
		.replace(/-+$/, ''); // Trim - from end of text

		return 'v-' + Date.now();
	};

	$('#video_title').keyup(function(event) {
		$('#video_slug').val(slugify($(this).val()));
	});

	$('#videos_list').selectpicker({
		style: 'btn-warning',
		size: 4
	});


	$('#video_form').submit(function(event) {
		event.preventDefault();
		$('#make_apps').text('Creating...').removeClass('btn-primary').addClass('btn-info');
		var title = $('#video_title').val();
		var slug = $('#video_slug').val();
		var keys = parseInt($('#video_keys').val(), 10);
		var views = parseInt($('#video_views').val(), 10);
		var video_file = $('#video_file').val();
		var video_output_folder = $('#video_output_folder').val();

		if (isValidInputs(title, slug, keys, views, video_file, video_output_folder)) {
			// TODO: atef implement file & folder stuff

			// console.info(video_output_folder, video_file);


			var fs = require("fs");
			var fse = require("fs-extra");
			var path = require("path");

			var imagePath = "gdata";
			var videoPath = video_file;
			var outputPath = video_output_folder;

			var imageBuffer = fs.readFileSync(imagePath);
			var infoBuffer = new Buffer(slug);
			// Lag will occaur here as the statment will block for a long time 
			// depending on the file size.
			// Use ReadStream to read the file in junks and to be able to show 
			// progress while creating.
			// Test first on sync case, and only show "Creating view..." message.
			var videoBuffer = fs.readFileSync(videoPath);

			var imageBufferSliced = imageBuffer.slice(11, imageBuffer.length);

			var buffers = [imageBufferSliced, infoBuffer, videoBuffer];

			var buffersLength = 0;
			for (var i = 0; i < buffers.length; ++i) {
				buffersLength += buffers[i].length;
			}

			var outputBuffer = Buffer.concat(buffers, buffersLength);


			fse.copy('app', outputPath, function(err) {
				if (err) swal('Error !', err, 'error');

				fse.copy('win_nwjs', outputPath, function(err) {
					if (err) swal('Error !', err, 'error');
					// console.info('success');

					server.newApp(title, slug, keys, views);

					fs.writeFileSync(outputPath + '/data', outputBuffer, 'hex');


					$('#make_apps').text('Submit').removeClass('btn-info').addClass('btn-primary');
				});

			});


		} else {
			swal('Error !', 'Check your inputs !', 'error');
		}
		$('#make_apps').text('Submit').removeClass('btn-info').addClass('btn-primary');
	});

	$('#refresh_list').click(function(e) {
		e.preventDefault();
		server.list_videos();
	});

	$('#key_generator_form').submit(function(event) {
		event.preventDefault();
		var slug = $('#videos_list').find(':selected').attr('value');
		var keys = parseInt($('#generator_keys').val(), 10);
		var views = parseInt($('#generator_views').val(), 10);
		server.generate_keys(slug, keys, views);
	});

	var isValidInputs = function(title, slug, keys, views) {
		return keys > 0 && views > 0;
	};

	var server = {
		newApp: function(title, slug, keys, views) {


			$.ajax({
				url: host + '/app/new',
				type: 'POST',
				crossDomain: true,
				data: {
					title: title,
					slug: slug,
					keys: keys,
					views: views
				},
				success: function(data) {

					var ks = $.parseJSON(data).keys;
					var keys_list = [];
					$.each(ks, function(index, val) {
						keys_list.push(val.license_key);
					});
					swal({
						title: 'Success !',
						text: 'List of available Keys:<hr> <textarea class="form-control" readonly="true" rows="' + keys_list.length + '">' + keys_list.join('\n') + '</textarea>',
						type: 'success',
						html: true
					});
				},
				error: function(err) {
					console.warn(err);
					swal('Error !', 'error in creating.', 'error');
				}
			});
		},

		list_videos: function() {
			$.get(host + '/video/list', function(data) {
				var videos_list = data;
				$('#videos_list').html('');
				$.each(videos_list, function(index, val) {
					$('#videos_list').append('<option value="' + val.slug + '" selected>' + val.name + '</option>');
					$('#videos_list').selectpicker('refresh');
				});

			});
		},

		generate_keys: function(slug, keys, views) {
			$.post(host + '/key/new', {
				slug: slug,
				keys: keys,
				views: views
			}, function(data, textStatus, xhr) {
				var ks = $.parseJSON(data).keys;
				var keys_list = [];
				$.each(ks, function(index, val) {
					keys_list.push(val.license_key);
				});
				swal({
					title: 'Success !',
					text: 'List of available Keys:<hr> <textarea class="form-control" readonly="true" rows="' + keys_list.length + '">' + keys_list.join('\n') + '</textarea>',
					type: 'success',
					html: true
				});
			}).fail(function(err) {
				swal('Error !', err, 'error');
			});
		}
	};
})();