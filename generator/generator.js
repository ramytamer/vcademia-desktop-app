var fs = require("fs");
var path = require("path");

var imagePath = path.resolve(__dirname, "gdata");
var videoPath = video_file;
var outputPath = path.resolve(video_output_folder, "data");

var imageBuffer = fs.readFileSync(imagePath);
var infoBuffer = new Buffer(slug);
// Lag will occaur here as the statment will block for a long time 
// depending on the file size.
// Use ReadStream to read the file in junks and to be able to show 
// progress while creating.
// Test first on sync case, and only show "Creating view..." message.
var videoBuffer = fs.readFileSync(videoPath);

var imageBufferSliced = imageBuffer.slice(11, imageBuffer.length);

var buffers = [imageBufferSliced, infoBuffer, videoBuffer];

var buffersLength = 0;
for (var i = 0; i < buffers.length; ++i) {
	buffersLength += buffers[i].length;
}

var outputBuffer = Buffer.concat(buffers, buffersLength);

fs.writeFileSync(outputPath, outputBuffer, 'hex');